const path = require('path');

module.exports = {
     entry: ["./src/js/index.js"],
     output: {
          path: path.resolve('./dist'),
          filename: "main.js",
          publicPath: '/'
     },
     module: {
          rules: [
               {
                    test: /\.scss$/,
                    use: [
                         "style-loader", // creates style nodes from JS strings
                         "css-loader", // translates CSS into CommonJS
                         "sass-loader" // compiles Sass to CSS, using Node Sass by default
                    ]
               }
          ],
     },
     resolve: {
          alias: {
               "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
          }
     }
};