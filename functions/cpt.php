<?php
/**
 * Created by PhpStorm.
 * User: juliengreletpro
 * Date: 01/01/2019
 * Time: 23:07
 */

// Custom post Typess
function cpt_projects() {
    $labels = array(
        'name'                => _x( 'Réalisations', 'Post Type General Name'),
        'singular_name'       => _x( 'Réalisation', 'Post Type Singular Name'),
        'menu_name'           => __( 'Réalisations'),
        'all_items'           => __( 'Toutes les réalisations'),
        'view_item'           => __( 'Voir les réalisations'),
        'add_new_item'        => __( 'Ajouter une réalisation'),
        'add_new'             => __( 'Ajouter une réalisation'),
        'edit_item'           => __( 'Editer la réalisation'),
        'update_item'         => __( 'Modifier la réalisation'),
        'search_items'        => __( 'Rechercher une réalisation'),
        'not_found'           => __( 'Non trouvée'),
        'not_found_in_trash'  => __( 'Non trouvée dans la corbeille')
    );

    $args = array(
        'label'               => __( 'Réalisations'),
        'description'         => __( 'Tous sur réalisations'),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'			  => array( 'slug' => 'realisations')
    );

    register_post_type( 'realisations', $args );

}

add_action( 'init', 'cpt_projects', 0 );
//add_action( 'init', 'timber', 0 );

// Taxonomies
function taxonomies_realisations() {
    $args_annee = array(
        'hierarchical'      => false,
        'labels'            => array(
            'name'              			=> _x( 'Catégories', 'taxonomy general name'),
            'singular_name'     			=> _x( 'Catégorie', 'taxonomy singular name'),
            'search_items'      			=> __( 'Chercher une categorie'),
            'all_items'        				=> __( 'Toutes les catégories'),
            'edit_item'         			=> __( 'Editer la catégorie'),
            'update_item'       			=> __( 'Mettre à jour la catégorie'),
            'add_new_item'     				=> __( 'Ajouter une catégorie'),
            'new_item_name'     			=> __( 'Valeur de la nouvelle catégorie'),
            'separate_items_with_commas'	=> __( 'Séparer les catégories avec une virgule'),
            'menu_name'                     => __( 'Categories'),
        ),
        'show_ui'                => true,
        'show_admin_column'      => true,
        'query_var'              => true,
        'rewrite'                => array( 'slug' => 'category_realisations' ),
    );
    register_taxonomy( 'category_realisations', 'realisations', $args_annee);
}

add_action( 'init', 'taxonomies_realisations', 0 );