<?php
/**
 * Created by PhpStorm.
 * User: juliengreletpro
 * Date: 02/01/2019
 * Time: 19:43
 */

if( function_exists('acf_add_options_page') &&
    function_exists('acf_add_options_sub_page') ) {

    acf_add_options_page( [
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ] );

    acf_add_options_sub_page( [
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings'
    ] );

}