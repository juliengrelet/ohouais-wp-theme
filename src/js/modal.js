// modal demande de devis
const modalDevis = document.getElementById("modal-devis");
const btnDevis = document.getElementById("btn-devis");
const btnCloseModalDevis = document.querySelector(".btn-close-modal");

btnDevis.addEventListener("click", () => {
     modalDevis.style.display = "flex";
     document.body.style.overflow = "hidden";
});

btnCloseModalDevis.addEventListener("click", () => {
     modalDevis.style.display = "none";
     document.body.style.overflow = "visible";
});