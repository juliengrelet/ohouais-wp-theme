// import modules
import { TweenMax } from "gsap/TweenMax";
import 'imports-loader?define=>false!animation.gsap';
import * as ScrollMagic from "scrollmagic";

// elements to animate
const title = document.querySelector('.big-title');
const image = document.querySelector('.image');
const text = document.querySelector('.content-about');
const bigTitleProject = document.querySelector('.big-title-project');

// animation
const animationTextRight = TweenMax.fromTo(title, 1, { x: 3000 }, { x: -3000 }, "start");
const animationImage = TweenMax.fromTo(image, 2, { autoAlpha: 0 }, { autoAlpha: 1 }, "start");
const animationtext = TweenMax.fromTo(text, 2, { y: 0 }, { y: 80 }, "start");
const animationTextLeft = TweenMax.fromTo(bigTitleProject, 1, { x: -3000 }, { x: 3000 }, "start");

// scenes
const controller = new ScrollMagic.Controller();

const newSceneScrollMagic = (triggerHook, triggerElement, duration, setTween) => {
     return new ScrollMagic.Scene({
          triggerHook: triggerHook,
          triggerElement: triggerElement,
          duration: duration
     })
         .setTween(setTween)
         .addTo(controller)
};

newSceneScrollMagic("onEnter", "#about-home", 2500, animationTextRight);
newSceneScrollMagic("onEnter", ".content-about", 800, animationtext);
newSceneScrollMagic("onEnter", ".image", 800, animationImage);
newSceneScrollMagic("onEnter", "#section-recent-projects", 2000, animationTextLeft);