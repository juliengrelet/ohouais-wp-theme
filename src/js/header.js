const header = document.querySelector(".header");

window.addEventListener( "scroll", () => {
     if(window.scrollY > 0){
          header.classList.add("sticky-header");
     } else {
          header.classList.remove("sticky-header");
     }
});

if( document.body.classList.contains('single-realisations') ){
     const titleSingle = document.querySelector('.title-detail-project');
     titleSingle.style.cursor = 'pointer';

     titleSingle.addEventListener('click', () => console.log('test'))
}