const urlAPI = "http://localhost:8888/ohouais-wp/wp-json";

// Call API WP
export const call = async () => {
     const dataAPIWp = await fetch(`${urlAPI}`)
         .then(response => response.json())
         .then(data => data);
     console.log(`API wordpress :`, dataAPIWp);
};

call();