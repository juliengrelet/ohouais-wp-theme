<?php
/*
 Template name: About
*/

$context = Timber::get_context();
$post = Timber::query_post();
$context["post"] = $post;
//var_dump($context["subTitle"]);

Timber::render( array('about-page.twig'), $context);
