<?php

$context = Timber::get_context();
$post = Timber::query_post();
$context['project'] = $post;

Timber::render( ['single-'.$post->post_type.'.twig'], $context );

