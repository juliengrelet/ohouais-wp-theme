<?php

$context = Timber::get_context();
$post = new TimberPost();
$context["post"] = $post;

// Get posts projects
$args = [
    'post_type' => 'realisations',
    'posts_per_page' => 1000,
    'order' => 'DESC'
];

$context['projects'] = Timber::get_posts($args);
$context['categories'] = Timber::get_terms('category_realisations');
// Render Timber
Timber::render(array("front-page.twig"), $context);
