<?php

$context = Timber::get_context();

$args = [
    'post_type' => 'realisations',
    'posts_per_page' => 100,
    'order' => 'DESC'
];

$context['projects'] = Timber::get_posts($args);

Timber::render(array("archive-realisations.twig"), $context);

